#!/usr/bin/bash

# This will get uncommented once we have the rpm
# make sure we have stalld installed
if rpm -q --quiet stalld; then
    :
else
    sudo dnf install -y stalld
    if [[ $? != 0 ]]; then
        echo "install of stalld failed!"
        exit 1
    fi
fi

STALLD="/usr/bin/stalld"

# See if stalld is installed and executable
$STALLD --help 2>> /dev/null
if [[ $? != 0 ]]; then
    exit 2
fi
